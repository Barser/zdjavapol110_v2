package pl.sda.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.sda.entity.Person;

public class PersonMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        session.save(new Person(null, "Michał", "Nowak", "12345678910"));


        transaction.commit();
        session.close();
        sessionFactory.close();

    }

}
