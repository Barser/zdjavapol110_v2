package pl.sda.manytomany;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Parent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;


    private String lastName;

    @ManyToMany(mappedBy = "parents")
    private List<Child> children;
}
