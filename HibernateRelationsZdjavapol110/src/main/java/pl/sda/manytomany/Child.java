package pl.sda.manytomany;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Child {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;

    private String lastName;

    @ManyToMany
    @JoinTable(
            name = "joinChildParent",
            joinColumns = @JoinColumn(name = "childId", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "parentId", referencedColumnName = "id")
    )
    private List<Parent> parents;

}
