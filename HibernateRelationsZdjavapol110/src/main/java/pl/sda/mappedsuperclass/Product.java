package pl.sda.mappedsuperclass;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@MappedSuperclass
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    protected String name;

    @Column(columnDefinition = "DECIMAL(7,2)")
    protected BigDecimal price;

    protected String kind;

    public Product() {

    }

    public Product(Integer id, String name, BigDecimal price, String kind) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.kind = kind;
    }
}
