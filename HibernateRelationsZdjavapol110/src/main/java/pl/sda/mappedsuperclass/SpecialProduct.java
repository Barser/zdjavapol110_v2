package pl.sda.mappedsuperclass;

import lombok.Data;


import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Data
public class SpecialProduct extends Product {

    private String description;

    public SpecialProduct() {

    }

    public SpecialProduct(Integer id, String name, BigDecimal price, String kind, String description) {
        super(id, name, price, kind);
        this.description = description;
    }
}
