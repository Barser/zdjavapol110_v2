package pl.sda.mappedsuperclass;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.math.BigDecimal;
import java.util.List;

public class ProductMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        final SpecialProduct specialProduct = new SpecialProduct(null, "special product",
                BigDecimal.valueOf(22222.55), "special", "desc");
        session.save(specialProduct);

        String hql = "FROM SpecialProduct WHERE description = :p1 AND id = :p2";
        final Query query = session.createQuery(hql);
        query.setParameter("p1", "desc");
        query.setParameter("p2", 1);

        final List<SpecialProduct> list = query.list();

        list.forEach(System.out::println);

        transaction.commit();
        session.close();
        sessionFactory.close();

    }

}
