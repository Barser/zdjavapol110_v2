package pl.sda.onetoone;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.time.LocalDate;

public class StudentMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        Student student = new Student(null, "Michał", "Nowak", LocalDate.now().minusYears(22), null);
        StudentIndex studentIndex = new StudentIndex(null, "12345", student);

        student.setStudentIndex(studentIndex);

        session.save(student);
        session.save(studentIndex);

        final StudentIndex studentIndexFromDB = session.get(StudentIndex.class, 1);

        System.out.println("Nazwisko studenta: " + studentIndexFromDB.getStudent().getLastName());

        transaction.commit();
        session.close();
        sessionFactory.close();


    }
}
