package pl.sda.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Person {

   private Integer id;

   private String firstName;

   private String lastName;

   private String pesel;

}
