package pl.sda.preparedstatement;

import pl.sda.util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCPreparedStatementExample {

    public static void main(String[] args) {

        try {
            final Connection connection = JDBCUtil.getConnection();
            String lastNameFromFrontend = "Nowak";
            int idFromFrontend = 2;
            String select = "SELECT * FROM person WHERE last_name = ? AND id > ?";

            final PreparedStatement preparedStatement = connection.prepareStatement(select);
            preparedStatement.setString(1, lastNameFromFrontend);
            preparedStatement.setInt(2, idFromFrontend);
            final ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                final int id = resultSet.getInt("id");
                final String firstName = resultSet.getString("first_name");
                final String lastName = resultSet.getString("last_name");
                final String pesel = resultSet.getString("pesel");

                System.out.println(id + " " + firstName + " " + lastName + " " + pesel);
            }

            connection.close();
            preparedStatement.close();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }

}
