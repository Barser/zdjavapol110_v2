package pl.sda.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.AfterAll;

import java.time.format.DateTimeFormatter;

public abstract class AbstractDaoTest {

    protected static SessionFactory sessionFactory =
            new Configuration().configure().buildSessionFactory();

    protected final String pesel = "12345678910";

    protected final String identificator = "123";

    protected final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");



}
