package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.sda.entity.Doctor;

import java.time.LocalDateTime;

public class DoctorDaoTest extends AbstractDaoTest {

    @Test
    public void testIsDoctorAvailableShouldReturnFalse() {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        DoctorDao doctorDao = new DoctorDao(session, Doctor.class);

        LocalDateTime dateTime = LocalDateTime.parse("2020-10-04 10:40", formatter);

        boolean result = doctorDao.isDoctorAvailable(identificator, dateTime, dateTime.plusHours(8));

        Assertions.assertFalse(result);

        transaction.commit();
        session.close();
    }

    @Test
    public void testIsDoctorAvailableShouldReturnFalse2() {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        DoctorDao doctorDao = new DoctorDao(session, Doctor.class);

        LocalDateTime dateTime = LocalDateTime.parse("2020-10-04 10:40", formatter);

        boolean result = doctorDao.isDoctorAvailable(identificator, dateTime, dateTime.plusHours(5));

        Assertions.assertFalse(result);

        transaction.commit();
        session.close();
    }

    @Test
    public void testIsDoctorAvailableShouldReturnTrue() {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        DoctorDao doctorDao = new DoctorDao(session, Doctor.class);

        LocalDateTime dateTime = LocalDateTime.parse("2020-10-04 08:00", formatter);

        boolean result = doctorDao.isDoctorAvailable(identificator, dateTime, dateTime.plusHours(2));

        Assertions.assertTrue(result);

        transaction.commit();
        session.close();
    }

    @AfterAll
    public static void shutdown() {
        sessionFactory.close();
    }


}
