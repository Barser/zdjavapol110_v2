package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.sda.entity.*;

import java.time.LocalDateTime;
import java.util.Arrays;

public class IntegrationDaoTest extends AbstractDaoTest {

    @AfterAll
    public static void shutdown() {
        sessionFactory.close();
    }

    @Test
    public void createRelatedObjects() {


        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        PatientDao patientDao = new PatientDao(session, Patient.class);
        AddressDao addressDao = new AddressDao(session, Address.class);
        DoctorDao doctorDao = new DoctorDao(session, Doctor.class);
        SpecialityDao specialityDao = new SpecialityDao(session, Speciality.class);
        AppointmentDao appointmentDao = new AppointmentDao(session, Appointment.class);

        Address address = new Address(null, "Warszawa", "Nowogrodzka", "56a");
        Patient patient = new Patient(null, "Michał", "Suwała",
                pesel, address, null);

        Speciality speciality = new Speciality(null, SpecialityType.INTERNISTA, null);
        Doctor doctor = new Doctor(null, "Jan", "Kowalski", identificator,
                null, Arrays.asList(speciality));
        speciality.setDoctors(Arrays.asList(doctor));

        LocalDateTime dateTime1 = LocalDateTime.parse("2020-10-04 11:00", formatter);
        LocalDateTime dateTime2 = LocalDateTime.parse("2020-10-04 13:40", formatter);
        LocalDateTime dateTime3 = LocalDateTime.parse("2020-10-04 17:20", formatter);

        Appointment appointment1 = new Appointment(null, dateTime1,
                dateTime1.plusMinutes(20), patient, doctor);

        Appointment appointment2 = new Appointment(null, dateTime2,
                dateTime2.plusMinutes(20), patient, doctor);

        Appointment appointment3 = new Appointment(null, dateTime3,
                dateTime3.plusMinutes(20), patient, doctor);

        patient.setAppointments(Arrays.asList(appointment1, appointment2, appointment3));
        doctor.setAppointments(Arrays.asList(appointment1, appointment2, appointment3));

        addressDao.save(address);
        patientDao.save(patient);
        specialityDao.save(speciality);
        doctorDao.save(doctor);
        appointmentDao.save(appointment1);
        appointmentDao.save(appointment2);
        appointmentDao.save(appointment3);

        Assertions.assertEquals(pesel, patientDao.getPatientByPesel(pesel).getPesel());

        transaction.commit();
        session.close();

    }

    @Test
    public void testGetFutureAppointmentsByPesel() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        PatientDao patientDao = new PatientDao(session, Patient.class);

        Assertions.assertEquals(3,
                patientDao.getFutureAppointmentsByPesel(pesel).size());

        transaction.commit();
        session.close();
    }


}
