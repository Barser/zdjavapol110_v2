package pl.sda.test;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.Test;



public class SessionFactoryTest {

    @Test
    public void test() {


        SessionFactory sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();

        sessionFactory.close();

    }

}
