package pl.sda.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Klasa reprezenruje zakres czasu.
 */
@Getter
@Setter
@AllArgsConstructor
public class Interval {

    private LocalDateTime start;
    private boolean inclusiveStart;
    private LocalDateTime end;
    private boolean inclusiveEnd;

    /**
     * Metoda sprawdza czy dwa zakresy czasu nakładają się na siebie.
     * Dodatkowo można określić za pomocą parametrów inclusive oraz exclusive
     * czy zakresy czasu mogą mieć elementy wspólne.
     * @param other - obiekt typu Interval, kóry reprezentuje zakres czasu.
     * @return
     * true jeśli dwa zakresy czasu nakłądają się na siebie,
     * false w przeciwnym wypadku.
     */
    public boolean overlaps(Interval other) {

        // intervals share at least one point in time
        if ((this.start.equals(other.getEnd())
                && this.inclusiveStart
                && other.isInclusiveEnd())
                ||
                (this.end.equals(other.getStart())
                && this.inclusiveEnd
                && other.isInclusiveStart())) {
            return true;
        }


        // intervals intersect
        if ((this.end.isAfter(other.getStart()) && this.start.isBefore(other.getStart()))
                || (other.getEnd().isAfter(this.start) && other.getStart().isBefore(this.start))) {
            return true;
        }

        // this interval contains the other interval
        if (((this.start.equals(other.getStart()) && other.isInclusiveStart())
                        || this.start.isAfter(other.getStart()))
                        &&
                        ((this.end.equals(other.getEnd()) && other.isInclusiveEnd())
                                || this.end.isBefore(other.getEnd()))) {
            return true;
        }


        // the other interval contains this interval
        if (((other.getStart().equals(this.start) && this.inclusiveStart)
                        || other.getStart().isAfter(this.start))
                        &&
                        ((other.end.equals(this.end) && this.inclusiveEnd)
                                ||
                                other.getEnd().isBefore(this.end))) {
            return true;
        }

        return false;
    }
}
