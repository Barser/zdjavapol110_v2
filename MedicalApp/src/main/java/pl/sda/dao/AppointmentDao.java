package pl.sda.dao;

import org.hibernate.Session;
import pl.sda.entity.Appointment;

public class AppointmentDao extends AbstractDao<Appointment> {

    public AppointmentDao(Session session, Class<Appointment> clazz) {
        super(session, clazz);
    }
}
