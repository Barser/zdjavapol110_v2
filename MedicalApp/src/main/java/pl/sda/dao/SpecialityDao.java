package pl.sda.dao;

import org.hibernate.Session;
import pl.sda.entity.Speciality;

public class SpecialityDao extends AbstractDao<Speciality> {

    public SpecialityDao(Session session, Class<Speciality> clazz) {
        super(session, clazz);
    }
}
