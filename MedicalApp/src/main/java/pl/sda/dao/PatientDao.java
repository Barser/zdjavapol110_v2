package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import pl.sda.entity.Appointment;
import pl.sda.entity.Patient;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PatientDao extends AbstractDao<Patient> {


    public PatientDao(Session session, Class<Patient> clazz) {
        super(session, clazz);
    }

    public Patient getPatientByPesel(String pesel) {

        CriteriaBuilder builder = session.getCriteriaBuilder();

        CriteriaQuery<Patient> query = builder.createQuery(clazz);

        Root<Patient> root = query.from(clazz);

        query.select(root).where(builder.equal(root.get("pesel"), pesel));

        Query<Patient> patientQuery = session.createQuery(query);

        return patientQuery.getResultStream()
                .findFirst()
                .orElseGet(null);
    }

    public List<Appointment> getFutureAppointmentsByPesel(String pesel) {

        Patient patient = getPatientByPesel(pesel);
        List<Appointment> appointments = new ArrayList<>();


        if (patient != null) {
            appointments = patient.getAppointments()
                    .stream()
                    .filter(a -> a.getDateFrom().isAfter(LocalDateTime.now()))
                    .collect(Collectors.toList());
        }

        return appointments;

    }
}
